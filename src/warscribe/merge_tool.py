import datetime
import logging
import logging.handlers
import math
import time
logger = logging.getLogger('merge-tool')

import json
import xml.etree.ElementTree as ET
from typing import List, Set

from warscribe.battlescribe import Catalog

def configureLogger(loggerName='merge-tool', level='DEBUG', logFormat='%(asctime)-15s \t [ %(module)-20s -> %(funcName)-40s -> %(threadName)-20s] [ %(levelname)-8s ] \t | %(message)s'):
    filename = 'logs/{loggerName}.log'.format(loggerName=loggerName)

    log_handler = logging.handlers.RotatingFileHandler(filename, backupCount=15)
    log_formatter = logging.Formatter(logFormat)
    log_formatter.converter = time.gmtime
    log_handler.setFormatter(log_formatter)
    log_handler.doRollover()

    logger.addHandler(log_handler)
    logger.setLevel(level)

    logger.info('Logger initialized')



def compare_xml_elements(a:ET.Element, b:ET.Element, ignore_ids=False):
    if (a == b):
        return True

    if (a is None or b is None):
        return False

    if (a.tag != b.tag or a.tail != b.tail or len(a.getchildren()) != len(b.getchildren())):
        return False

    if (ignore_ids and 'id' in a.attrib and 'id' in b.attrib):
        a_id = a.attrib['id']
        a.attrib['id'] = b.attrib['id']
        result = a.attrib != b.attrib
        a.attrib['id'] = a_id
        
        if not (result):
            return False
            
    elif (a.attrib != b.attrib):
        return False

    for i in range(len(a.getchildren())):
        if not (compare_xml_elements(a[i], b[i], ignore_ids=ignore_ids)):
            return False

    #must be equal
    return True



def find_ids(cat:Catalog):
    return [ element.attrib['id'] for element in cat.root.findall(".//*[@id]") ]

def resolve_id_conflicts(cats:List[Catalog], config):
    id_lists = [find_ids(cat) for cat in cats]
    resulting_ids = []

    config_path = 'configs/{dynamic_config}'.format(**config)
    try:
        file = open(config_path, 'r')
        dynamic_config = json.load(file)
        file.close()
    except:
        dynamic_config = {}

    for i in range(len(cats)):
        id_list = id_lists[i]
        cat = cats[i]
        conflicts = set(id_list).intersection(resulting_ids)

        for conflict in conflicts:
            conflicing_node_new = cat.find_id(conflict)
            conflicing_cat_new = cat

            conflicing_node_old = None
            conflicing_cat_old = None
            for j in range(i):
                test = cats[j].find_id(conflict)
                if not (test is None):
                    conflicing_node_old = test
                    conflicing_cat_old = cats[j]
                    break

            print("Conflicting id: {id}".format(id=conflict))
            print("  Element 1: {type} ('{name}' from '{catname}')".format(catname=conflicing_cat_old.root.attrib['name'], type=conflicing_node_old.tag, name=conflicing_node_old.attrib['name'] if 'name' in conflicing_node_old.attrib else 'no name'))
            print("  Element 2: {type} ('{name}' from '{catname}')".format(catname=conflicing_cat_new.root.attrib['name'], type=conflicing_node_new.tag, name=conflicing_node_new.attrib['name'] if 'name' in conflicing_node_new.attrib else 'no name'))

            if (compare_xml_elements(conflicing_node_new, conflicing_node_old)):
                print("  Resolution: same, merge 'as is'")
                cat.remove_from_parent(conflicing_node_new)

            else:
                print("  Resolution: different, rename second")

                if not ('id_conflicts' in dynamic_config):
                    dynamic_config['id_conflicts'] = {}

                new_id = None
                if (conflict in dynamic_config['id_conflicts']):
                    loaded = dynamic_config['id_conflicts'][conflict]
                    if (
                    not (loaded['new_id'] in resulting_ids or loaded['new_id'] in id_list)
                    and loaded['new_cat_id'] == conflicing_cat_new.root.attrib['id']
                    and loaded['old_cat_id'] == conflicing_cat_old.root.attrib['id']
                    and loaded['new_type'] == conflicing_node_new.tag
                    and loaded['old_type'] == conflicing_node_old.tag
                    and loaded['new_name'] == conflicing_node_new.attrib['name']
                    and loaded['old_name'] == conflicing_node_old.attrib['name']
                    ):
                        new_id = loaded['new_id']
                        print("  New id: {id} (loaded from previous runs)".format(id=new_id))

                if (new_id is None):
                    new_id = cat.generate_id()

                    dynamic_config['id_conflicts'][conflict] = \
                    {
                        'new_cat_id': conflicing_cat_new.root.attrib['id'],
                        'old_cat_id': conflicing_cat_old.root.attrib['id'],
                        'new_type': conflicing_node_new.tag,
                        'old_type': conflicing_node_old.tag,
                        'new_name': conflicing_node_new.attrib['name'],
                        'old_name': conflicing_node_old.attrib['name'],
                        'new_id': new_id
                    }
                    print("  New id: {id}".format(id=new_id))

                cat.replace_id(conflict, new_id)
                id_list.remove(conflict)
                id_list.append(new_id)

            print('')

        resulting_ids += id_list

    file = open(config_path, 'w')
    file.write(json.dumps(dynamic_config, indent=4, sort_keys=True))
    file.close()

def merge(cats:List[Catalog], config) -> Catalog:

    config_path = 'configs/{dynamic_config}'.format(**config)
    try:
        file = open(config_path, 'r')
        dynamic_config = json.load(file)
        file.close()
    except:
        dynamic_config = {}

    result = cats[0]
    for i in range(1, len(cats)):
        cat = cats[i]

        for root_child in cat.root:
            result_child = result.root.find("./{full_tag}".format(full_tag=root_child.tag))
            for root_child_2 in root_child:
                result_child.append(cat.get_duplicate(root_child_2, replace_ids=False))

    new_id = None
    if ('new_id' in dynamic_config):
        new_id = dynamic_config['new_id']
        print("New catalog id: {id} (loaded from previous runs)".format(id=new_id))
    else:
        new_id = result.generate_id()
        dynamic_config['new_id'] = new_id
        print("New catalog id: {id}".format(id=new_id))

    result.replace_id(result.root.attrib['id'], new_id=new_id)

    file = open(config_path, 'w')
    file.write(json.dumps(dynamic_config, indent=4, sort_keys=True))
    file.close()

    return result

def delete_recursive(cat:Catalog, id:str, hard:bool=False, except_list:List[str]=[]):
    node = cat.find_id(id)
    if (except_list and 'name' in node.attrib and node.attrib['name'] in except_list):
        print("Skipping deletion of '{name}' (listed in exceptions list)".format(**node.attrib))
        return

    if ('name' in node.attrib and not(node.attrib['name'] == '' or node.attrib['name'] == 'New InfoLink' or node.attrib['name'] == 'New ProfileLink' or node.attrib['name'] == 'New EntryLink')):
        print("Deleting '{name}'".format(**node.attrib))
    else:
        print("Deleting {tag} {id}".format(id=id, tag=node.tag))

    parent = cat.find_parent(node)
    parent.remove(node)

    # NOT findall 'cause the list can change upon deleting
    query = ".//*[@targetId='{id}']".format(id=id)
    while True:
        result = cat.root.find(query)
        if (result is None):
            break;
        delete_recursive(cat, result.attrib['id'], hard=hard, except_list=except_list)

    if (hard):
        while(True):
            parent_of_parent = cat.find_parent(parent)
            if (parent_of_parent == cat.root):
                break;
            if (except_list and 'name' in parent_of_parent.attrib and parent_of_parent.attrib['name'] in except_list):
                break;

            if ('id' in parent.attrib):
                delete_recursive(cat, parent.attrib['id'], hard=hard, except_list=except_list)
                break;
            else:
                print("Deleting {tag}".format(tag=parent.tag))
                parent_of_parent.remove(parent)
                parent = parent_of_parent


def parse_xpath(cat:Catalog, action) -> str:
    if ('xpath' in action):
        xpath = action['xpath']
    else:
        if ('type' in action):
            xpath = ".//{actionType}".format(actionType=action['type'])
        else:
            xpath = ".//*"

        if ('id' in action):
            xpath += "[@id='{id}']".format(id=action['id'])
        if ('name' in action):
            xpath += "[@name='{name}']".format(name=action['name'])
        if ('attributes' in action):
            for attribute in action['attributes']:
                xpath += "[@{attribute}]".format(attribute=attribute)

    return xpath

def merge_nodes(cat:Catalog, ids:List[str]):

    ids = sorted(ids)
    for i in range(1, len(ids)):
        merging = cat.find_id(ids[i])
        print("Merging {name}: {id1} and {id2}".format(name='\'' + merging.attrib['name'] + '\'' if 'name' in merging.attrib else merging.tag, id1=ids[0], id2=ids[i]))
        cat.remove_from_parent(merging)
        cat.replace_id(ids[i], new_id=ids[0])

def merge_check(cat:Catalog, nodes:List[ET.Element], attributes:List[str], children:List[str]=[]) -> List[List[ET.Element]]:
    result = []

    for node in nodes:
        appended_into_group = False
        for group in result:
            first_node_in_group = group[0]

            if (cat.find_parent(node) == cat.find_parent(first_node_in_group)):
                check_failed = False
                for attribute in attributes:
                    if (node.attrib[attribute] != first_node_in_group.attrib[attribute]):
                        check_failed = True
                        break;
                for child_tag in children:
                    if (child_tag.startswith('!')):
                        tag = child_tag[1:]
                        ignore_ids = True
                    else:
                        tag = child_tag
                        ignore_ids = False

                    child_node = node.find(tag)
                    first_node_child = first_node_in_group.find(tag)
                    if (not compare_xml_elements(child_node, first_node_child, ignore_ids=ignore_ids)):
                        check_failed = True
                        break;

                if not (check_failed):
                    appended_into_group = True
                    group.append(node)
                    break;

        if not (appended_into_group):
            result.append([ node ])

    return result

def parse_condition(cat:Catalog, condition) -> Set[ET.Element]:

    if (condition['conditionType'] == 'and'):
        result = set()
        for subcondition in condition['conditions']:
            subresult = parse_condition(cat, subcondition)
            result = result.intersection(subresult)

    elif (condition['conditionType'] == 'or'):
        result = []
        for subcondition in condition['conditions']:
            subresult = parse_condition(cat, subcondition)
            result += list(subresult)
        result = set(result)

    elif (condition['conditionType'] == 'hasLink'):
        target = cat.find_link_target_by_xpath(parse_xpath(cat, condition['target']))
        print("Condition hasLink: target: '{targetName}' ({targetId})".format(targetId=target.attrib['id'], targetName=target.attrib['name']))
        result = set(cat.root.findall(".//*[@targetId='{targetId}']/../..".format(targetId=target.attrib['id'])))

    else:
        result = set([])

    print("Condition {conditionType}:\t{n} found".format(conditionType=condition['conditionType'], n=len(result)))
    return result

def post_merge(cat:Catalog, config):

    config_path = 'configs/{dynamic_config}'.format(**config)
    try:
        file = open(config_path, 'r')
        dynamic_config = json.load(file)
        file.close()
    except:
        dynamic_config = {}

    actions_path = 'configs/{actions}'.format(**config)
    try:
        file = open(actions_path, 'r')
        actions = json.load(file)
        file.close()
    except:
        actions = []

    for action in actions:
        if (action['action'].startswith('remove')):
            xpath = parse_xpath(cat, action)

            while True:
                query_result = cat.root.find(xpath)
                if (query_result is None):
                    break
                if (not 'except' in action or action['except'] is None):
                    action['except'] = None

                delete_recursive(cat, query_result.attrib['id'], hard='hard' in action['action'], except_list=action['except'])

        elif (action['action'].startswith('merge')):

            xpath = parse_xpath(cat, action)
            results = sorted(cat.root.findall(xpath), key=lambda node: node.attrib[action['attributes'][0]])
            same_attrib_1 = None
            for i in range(1, len(results)):
                if (results[i].attrib[action['attributes'][0]] == results[i - 1].attrib[action['attributes'][0]]):
                    if (same_attrib_1 is None):
                        same_attrib_1 = [ results[i-1] ]
                    same_attrib_1.append(results[i])
                else:
                    if not (same_attrib_1 is None):
                        groups = merge_check(cat, same_attrib_1, action['attributes'], 'children' in action and action['children'] or [])
                        for group in groups:
                            merge_nodes(cat, [ node.attrib['id'] for node in group ] )

                        same_attrib_1 = None

            if not (same_attrib_1 is None):
                groups = merge_check(cat, same_attrib_1, action['attributes'])
                for group in groups:
                    merge_nodes(cat, [node.attrib['id'] for node in group])

        elif (action['action'].startswith('link')):
            if not ("link_ids" in dynamic_config):
                dynamic_config['link_ids'] = { }

            target = cat.find_link_target_by_xpath(parse_xpath(cat, action['target']))
            targetId = target.attrib['id']
            sources = parse_condition(cat, action['condition'])
            link_type = cat.get_link_type(target)

            if not (targetId in dynamic_config['link_ids']):
                dynamic_config['link_ids'][targetId] = {}

            for node in sources:
                if (node.find(".//*[@targetId='{targetId}']/..".format(targetId=targetId)) is None):
                    nodeId = node.attrib['id']

                    print("Creating link to '{targetName}' for '{sourceName}' ({sourceId})".format(targetName=target.attrib['name'], sourceName=node.attrib['name'], sourceId=nodeId))
                    link = ET.SubElement(node.find(link_type + 's'), link_type)
                    if (nodeId in dynamic_config['link_ids'][targetId]):
                        link.attrib['id'] = dynamic_config['link_ids'][targetId][nodeId]
                        print("  Link id: {id} (loaded from previous runs)".format(id=link.attrib['id']))
                    else:
                        link.attrib['id'] = cat.generate_id()
                        dynamic_config['link_ids'][targetId][nodeId] = link.attrib['id']
                        print("  Link id: {id}".format(id=link.attrib['id']))

                    link.attrib['targetId'] = targetId
                    link.attrib['type'] = target.tag
                    if ('name' in target.attrib and target.attrib['name'] != ''):
                        link.attrib['name'] = target.attrib['name']
                    else:
                        link.attrib['name'] = "New " + link.tag.capitalize()
                    link.attrib['hidden'] = "false"
        
        else:
            print("test")

    file = open(config_path, 'w')
    file.write(json.dumps(dynamic_config, indent=4, sort_keys=True))
    file.close()

def main():
    config_path = 'configs/merge-tool-config.json'
    try:
        file = open(config_path, 'r')
        config = json.load(file)
        file.close()
    except FileNotFoundError:
        raise FileNotFoundError ("Configuration file not found!")
    except json.JSONDecodeError as exc:
        raise json.JSONDecodeError ("Configuration file corrupted!", doc=exc.doc, pos=exc.pos)

    if ('directory' in config and not config['directory'] is None and config['directory'] != 'default'):
        dir = config['directory']
    else:
        dir = None

    cat_names = config['catalogs']

    if ('resultName' in config and not config['resultName'] is None):
        resultName = config['resultName']
    else:
        resultName = "{cat_name} (merged)".format(cat_name=cat_names[0])

    cats = [Catalog.find(directory=dir, name=cat_name, recursive=True) for cat_name in cat_names]


    resolve_id_conflicts(cats, config)
    result = merge(cats, config)
    post_merge(result, config)

    result.save(dir + "/{resultName}.cat".format(resultName=resultName))
    return 0

if (__name__ == "__main__"):
    start_time = datetime.datetime.now()
    # # Logger disabled
    # configureLogger()
    result = main()
    dt = datetime.datetime.now() - start_time
    if (dt.total_seconds() > 1.5):
        print("Time spent: {minutes}:{seconds:02}".format(minutes=int(math.floor(dt.seconds % 3600 / 60)), seconds=dt.seconds % 60))
    else:
        print("Time spent: {msec}ms".format(msec=int(dt.microseconds % 10000000 / 1000)))
    exit(result)

