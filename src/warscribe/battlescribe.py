import sys, os, glob
import time, datetime, math

import argparse, logging, logging.handlers
logger = logging.getLogger('BS-file-loader')

import xml.etree.ElementTree as ET
import re
import random
import copy
import typing

def configureLogger(loggerName='BS-file-loader', level='DEBUG', logFormat='%(asctime)-15s \t [ %(module)-20s -> %(funcName)-40s -> %(threadName)-20s] [ %(levelname)-8s ] \t | %(message)s'):
    filename = 'logs/{loggerName}.log'.format(loggerName=loggerName)

    log_handler = logging.handlers.RotatingFileHandler(filename, backupCount=15)
    log_formatter = logging.Formatter(logFormat)
    log_formatter.converter = time.gmtime
    log_handler.setFormatter(log_formatter)
    log_handler.doRollover()

    logger.addHandler(log_handler)
    logger.setLevel(level)

    logger.info('Logger initialized')

def get_default_directory():
    path_suffix = "{0}/BattleScribe/data"
    if (os.name == 'nt'):
        return path_suffix.format((os.environ['HOMEDRIVE'] + os.environ['HOMEPATH']).replace('\\\\', '\\'))
    elif (os.name == 'posix'):
        return path_suffix.format(os.environ['HOME'])
    else:
        return ".."

class BS_file:

    __xml_tree = None
    root = None
    __path = None
    _default_directory = None


    def __init__(self, path, default_directory=None):
        if not (os.path.exists(path) and os.path.isfile(path)):
            raise FileNotFoundError("Cannot find file {path}".format(path=path))

        self.__path = path
        self._default_directory = default_directory
        filename, file_extension = os.path.splitext(path)

        if (file_extension.endswith('z') and len(file_extension) > 3):
            import zipfile
            with zipfile.ZipFile(path, 'r') as catz:
                cat_info = catz.infolist()[0]
                cat_file = catz.open(cat_info)
                file_content = cat_file.readlines()
                self.__xml_tree = ET.ElementTree(ET.fromstringlist(file_content))

        else:
            self.__xml_tree = ET.parse(path)

        self.root = self.__xml_tree.getroot()

        xml_namespace = re.match('\{.*\}', self.root.tag).group(0)
        all_nodes = self.root.findall(".//*")
        all_nodes.append(self.root)
        for node in all_nodes:
            node.tag = node.tag.replace(xml_namespace, '')

    def find_id(self, id):
        return self.root.find(".//*[@id='{id}']".format(id=id))

    def find_parent_of_parent(self, element):
        if ('id' in element.attrib):
            return self.root.find(".//*[@id='{id}']/../..".format(id=element.attrib['id']))
        else:
            return self.find_parent(self.find_parent(element))

    def find_link_target_by_xpath(self, xpath:str) -> ET.Element:
        return self.root.find(xpath)

    def find_parent(self, element:ET.Element) -> ET.Element:
        if ('id' in element.attrib):
            return self.root.find(".//*[@id='{id}']/..".format(id=element.attrib['id']))
        else:
            return next((n for n in self.root.findall(".//{type}/..".format(type=element.tag)) if element in n), None)

    def remove_from_parent(self, element):
        self.find_parent(element).remove(element)

    def add_on_same_level(self, old_element, new_element):
        self.find_parent(old_element).append(new_element)

    def save(self, path=""):
        if (path):
            self.path = path

        if not (path.endswith('z')):
            self.__xml_tree.write(path)
        else:
            raise NotImplemented("Saving as zipped is not implemented yet")

    def get_all_ids(self):
        return set(node.attrib['id'] for node in self.root.findall(".//*[@id]"))

    def generate_id(self, format="{0:04x}-{1:04x}-{2:04x}-{3:04x}", excludes=[]):
        excludes = set(excludes)
        excludes.update(self.get_all_ids())

        while True:
            id = self.__generate_id(format)
            if not (excludes and id in excludes):
                return id

    def __generate_id(self, format):
        rand_list = [random.randint(0, 0x10000) for i in range(12)]
        return format.format(*rand_list)


    def replace_id(self, id, new_id = None, parent_node=None):
        if (parent_node is None):
            parent_node = self.root

        if (new_id is None):
            new_id = self.generate_id()

        # elem = self.find_id(id)
        # if not (elem is None):
        #     elem.attrib['id'] = new_id

        elem_list = parent_node.findall('.//')
        elem_list.append(parent_node)

        for elem in elem_list:
            for attrib_name in elem.attrib:
                if (elem.attrib[attrib_name] == id):
                    elem.attrib[attrib_name] = new_id

        return new_id
    
    def get_duplicate(self, node, replace_ids=True):
        if (node == self.root):
            raise ValueError('Cannot duplicate root element')

        new_node = copy.deepcopy(node)

        if (replace_ids):
            elem_list = new_node.findall(".//*[@id]")
            if ('id' in new_node.attrib):
                elem_list.append(new_node)

            for elem in elem_list:
                self.replace_id(elem.attrib['id'], parent_node=new_node)

        return new_node

    def duplicate_node(self, node):
        new_node = self.get_duplicate(node)
        self.add_on_same_level(node, new_node)
        return new_node

    def replace_attrib(self, attrib_name, old_value, new_value, parent_node=None, partial=False):
        if (parent_node is None):
            parent_node = self.root

        if (not partial):
            elem_list = parent_node.findall(".//*[@{attrib_name}='{value}']".format(attrib_name=attrib_name, value=old_value))
            if (attrib_name in parent_node.attrib and parent_node.attrib[attrib_name] == old_value):
                elem_list.append(parent_node)
            for elem in elem_list:
                elem.attrib[attrib_name] = new_value

        else:
            elem_list = [ x for x in parent_node.findall(".//*[@{attrib_name}]".format(attrib_name=attrib_name)) 
                          if (old_value in x.attrib[attrib_name]) ]
            for elem in elem_list:
                elem.attrib[attrib_name] = elem.attrib[attrib_name].replace(old_value, new_value)


class Catalog(BS_file):

    @staticmethod
    def find(name:str, directory:str=None, recursive:bool=True) -> 'Catalog':
        pattern = name

        if (directory is None):
            directory = get_default_directory()

        if not (pattern.endswith(('.cat', '.catz'))):
            pattern += '.cat'
        if (os.path.isabs(pattern)):
            absolute_path = True
            directory = ''
        else:
            absolute_path = False
            pattern = '/' + pattern

        if (os.path.isfile(directory + pattern)):
            return Catalog(directory + pattern, default_directory=directory)
        if (os.path.isfile(directory + pattern + 'z')):
            return Catalog(directory + pattern + 'z', default_directory=directory)
        if (recursive and not absolute_path):
            catalogs = list(set(glob.glob(directory + '/**/' + pattern) + glob.glob(directory + '/**/' + pattern + 'z')))
            if (len(catalogs) == 1):
                return Catalog(catalogs[0], default_directory=directory)
            elif (len(catalogs) > 1):
                print ("Two or more potential catalogs found!")
                print(catalogs)
                raise FileExistsError("Two or more potential catalogs found!\n{catalogs}".format(catalogs=catalogs))

        raise FileNotFoundError("No file found: {name}".format(name=name))

    @staticmethod
    def get_link_type(target)->str:
        if (target.tag == 'selectionEntry'):
            return 'entryLink'
        if (target.tag == 'selectionEntryGroup'):
            return 'entryLink'

        if (target.tag == 'categoryEntry'):
            return 'categoryLink'

        if (target.tag == 'profile'):
            return 'infoLink'
        if (target.tag == 'rule'):
            return 'infoLink'

        return None

    __parent_gst = None

    sharedSelectionEntries = None
    sharedProfiles = None

    def find_game_system(self) -> 'Catalog':
        dir = self._default_directory or os.path.dirname(self._BS_file__path)
        game_systems = set(glob.glob(dir + '/*.gst') + glob.glob(dir + '/**/*.gst'))
        for gst_file in game_systems:
            gst = Catalog(gst_file, default_directory=dir)
            if (gst.root.attrib['id'] == self.root.attrib['gameSystemId']):
                self.__parent_gst = gst
                return gst

        self.__parent_gst = None
        return None


    def __init__(self, path, default_directory):
        BS_file.__init__(self, path, default_directory)

        self.sharedSelectionEntries = self.root.find('sharedSelectionEntries')
        self.sharedProfiles = self.root.find('sharedProfiles')

    def find_link_target_by_xpath(self, xpath: str) -> ET.Element:
        test = BS_file.find_link_target_by_xpath(self, xpath)
        if not (test is None):
            return test
        elif (self.find_game_system()):
            test = self.__parent_gst.root.find(xpath)
            return test
        else:
            return None

    def generate_id(self, format="{0:04x}-{1:04x}-{2:04x}-{3:04x}", excludes=[]):

        excludes = set(excludes)

        if (self.__parent_gst is None):
            self.find_game_system()
        if (self.__parent_gst):
            parend_ids = self.__parent_gst.get_all_ids()
            excludes.update(parend_ids)

        return BS_file.generate_id(self, format=format, excludes=excludes)


def main():
    cat = Catalog('/home/peter/git/wh40k/data/Aeldari - Harlequins.cat')
    gst = cat.find_game_system()
    for attr_name in gst.root.attrib:
        print('{0}: {1}'.format(attr_name, gst.root.attrib[attr_name]))

    print(len(cat.get_all_ids()))
    print(list(cat.get_all_ids())[5])
    return 0


if (__name__ == "__main__"):
    start_time = datetime.datetime.now()
    # # Logger disabled
    # configureLogger()
    result = main()
    dt = datetime.datetime.now() - start_time
    if (dt.total_seconds() > 1.5):
        print("Time spent: {minutes}:{seconds:02}".format(minutes=int(math.floor(dt.seconds % 3600 / 60)), seconds=dt.seconds % 60))
    else:
        print("Time spent: {msec}ms".format(msec=int(dt.microseconds % 10000000 / 1000)))
    exit(result)
