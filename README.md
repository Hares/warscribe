Merge Tool
==========

### How to:
1. Make sure Python 3 or higher is installed. Can download from [here](https://www.python.org/downloads/)
2. File `merge_tool_configs/config.json` is required to work. Find required config (`ynnari.config.json`, as example), and copy it into `config.json`, or create a new one. Then open it as text file and check:
  * Check catalogs to be merged - `catalogs` variable (first one is target, orhers will be merged into). Case-sensitive in Linux, extensions are added automatically.
  * Check directory catalogs are stored - `directory` variable (in double quotmarks). Case-sensitive in Linux. If missing (or `null`, or `"default"`), tool will try to find catalogs in the default BattleScribe directory, including all subdirectories.
  * Check name of resulting file - `resultName`. Extensions are added automatically.
  * `actions` field should link to the file listing the post-merge actions
  * `dynamic_config` field should link to the file where all dynamic information is stored
3. If required, can check & edit `merge_tool/actions.json` file.
4. Run `merge_tool.py`
5. Due to internal issues, resulting catalog is recommended to be opened in data editor and saved. Otherwise it wound work, but there will be difference in about any line due to xml node attributes sorted by name.

### What it does:
1. It opens listed catalogs and merges them into first.
2. It resolves all id conflicts.
3. If any ids are to be generated, it will check if id was generated in previous runs, so elements will have same id if run this tool several times.
4. It reads config listed in `actions` field (`ynnari.actions.json`, for example) and reproduces its steps in order. In example, it can merge all enrties with same name, type and shared status.

### Ynnari config:
0. **Finds files in the default BattleScribe directory and all subdirectories.**
1. Merges "Aeldari - Halequins", "Aeldari - Craftworlds" and "Aeldari - Drukhari" into "Aeldari - Ynnari".
2. All links to unallowed units removed (Haemonculus coven, i.e.).
3. All categories with same name merged.
4. All entries with same name, shared status and type merged.
5. All links to 4 faction rules removed (Ancient Doom, Battle Focus, Power from Pain, Rising Crescendo).
6. Links to "Strength from Death" added for all bikers and infantry units.
7. Saves as "Aeldari - Ynnari (merged)"

### Supported actions:
1. `remove` - removes all instances that match condition, as well as links to them.
2. `remove hard` - same as above + removes anything that contains removed items.
3. `merge` - merge elements those match condition and have same attributes. Element with first Id in alphabetical order will be used. Note that this operation is not fast enough.
4. `link` - add link to specific element for elements those match very specific condition.


Other Usage
===========
Please, contact author for more support. Documentation is not provided yet.