import datetime
import logging
import logging.handlers
import math
import os
import time
logger = logging.getLogger('catalogue-modifier')

import xml.etree.ElementTree as ET

from warscribe.battlescribe import Catalog

def configureLogger(loggerName='catalogue-modifier', level='DEBUG', logFormat='%(asctime)-15s \t [ %(module)-20s -> %(funcName)-40s -> %(threadName)-20s] [ %(levelname)-8s ] \t | %(message)s'):
    filename = 'logs/{loggerName}.log'.format(loggerName=loggerName)

    log_handler = logging.handlers.RotatingFileHandler(filename, backupCount=15)
    log_formatter = logging.Formatter(logFormat)
    log_formatter.converter = time.gmtime
    log_handler.setFormatter(log_formatter)
    log_handler.doRollover()

    logger.addHandler(log_handler)
    logger.setLevel(level)

    logger.info('Logger initialized')

def remove_links(cat:Catalog):
    for entry in cat.sharedSelectionEntries:
        links = entry.findall('{type}s/{type}'.format(type='infoLink'))

        for link in links:
            target = cat.find_id(link.attrib['targetId'])
            cat.remove_from_parent(target)
            cat.remove_from_parent(link)
            entry.find(target.tag + 's').append(target)


def add_characters(cat:Catalog, source_char_name, char_list):
    source_char = cat.root.find(".//{type}[@name='{name}']".format(type='selectionEntry', name=source_char_name))
    for char_name in char_list:
        new_char = cat.duplicate_node(source_char)
        cat.replace_attrib('name', old_value=source_char_name, new_value=char_name, parent_node=new_char, partial=True)

def copy_options(cat:Catalog, source_char_name, char_list, ignore_profiles=False):
    source_char = cat.root.find(".//{type}[@name='{name}']".format(type='selectionEntry', name=source_char_name))
    for char_name in char_list:
        dest_char = cat.root.find(".//{type}[@name='{name}']".format(type='selectionEntry', name=char_name))
        for child_node_group in source_char:
            dest_node_group = dest_char.find("./{type}".format(type=child_node_group.tag))
            for child in child_node_group:
                if (child.tag == 'cost'):
                    continue
                if (('name' in child.attrib) and (child.attrib['name'] in source_char_name)):
                    continue
                if (ignore_profiles and (child.tag == 'profile')):
                    continue

                duplicate = cat.get_duplicate(child)
                cat.replace_id(source_char.attrib['id'], new_id=dest_char.attrib['id'], parent_node=duplicate)
                dest_node_group.append(duplicate)

def rules_to_profiles(cat:Catalog, rules_list='all', root=None, rules_exceptions=[]):
    if (root is None):
        root = cat.root

    if (rules_list == 'all'):
        node_list = root.findall(".//{type}[@name]".format(type='rule'))
    else:
        node_list = [root.find(".//{type}[@name='{name}']".format(type='rule', name=rule_name)) for rule_name in rules_list]

    for node in node_list:
        if (node.attrib['name'] in rules_exceptions):
            continue
        node.tag = "{type}".format(type='profile')

        parent = cat.find_parent(node)

        new_parent = cat.find_parent_of_parent(node).find("./{type}".format(type=parent.tag.replace('rule', 'profile').replace('Rule', 'Profile')))
        parent.remove(node)

        node.attrib['profileTypeId']="72c5eafc-75bf-4ed9-b425-78009f1efe82"
        node.attrib['profileTypeName']="Abilities"

        description_node = node.find("{type}".format(type='description'))

        # print(description_node.text)

        characteristics_node = ET.SubElement(node, "{type}".format(type='characteristics'))
        description_characteristic = ET.SubElement(characteristics_node, "{type}".format(type='characteristic'))
        description_characteristic.attrib['name'] = "Description"
        description_characteristic.attrib['characteristicTypeId'] = "21befb24-fc85-4f52-a745-64b2e48f8228"
        description_characteristic.attrib['value'] = description_node.text

        for link in cat.root.findall(".//{type}[@targetId='{id}'][@type='rule']".format(type='infoLink', id=node.attrib['id'])):
            link.attrib['type'] = 'profile'

        node.remove(description_node)
        new_parent.append(node)

def main():
    if (os.name == 'nt'):
        dir = "D:/Code/sync/wh40k/data/Warhammer 40,000 8th Edition"
    elif (os.name == 'posix'):
        dir = "/home/peter/git/wh40k/data"
    else:
        dir = '..'

    codex = 'Craftworlds'

    cat = Catalog(dir + "/{codex}.cat".format(codex=codex))
    basic_char_name = 'Falcon'
    char_list = ['Fire Prism', 'Night Spinner', 'Wave Serpent']
    copy_options(cat, basic_char_name, char_list, ignore_profiles=True)
    #rules_to_profiles(cat, rules_exceptions=["Rising Cresendo"])

    cat.save(dir + "/{codex}_upd.cat".format(codex=codex))
    return 0

if (__name__ == "__main__"):
    start_time = datetime.datetime.now()
    # # Logger disabled
    # configureLogger()
    result = main()
    dt = datetime.datetime.now() - start_time
    if (dt.total_seconds() > 1.5):
        print("Time spent: {minutes}:{seconds:02}".format(minutes=int(math.floor(dt.seconds % 3600 / 60)), seconds=dt.seconds % 60))
    else:
        print("Time spent: {msec}ms".format(msec=int(dt.microseconds % 10000000 / 1000)))
    exit(result)

